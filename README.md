# The use of artificial neural networks to identify decision-making models

The author of RBF-implementation is Petra Vidnerová, which is provided on [github](https://github.com/PetraVidnerova/rbf_for_tf2), under the MIT license.

The main objective of the project was to demonstrate the possibility of using artificial neural networks to identify decision-making models whose method of formation has been lost. For this project to work, the data needs to be generated which can be done through provided scripts. Moreover, the decision-making models are based on the COMET, which is multic-criteria decision-making method based on fuzzy rules to create the decision model. 

## Abstract

The growing popularity of multi-criteria decision-making methods created a need to
continuously develop and improve the available solutions. Moreover, such methods are
increasingly being applied to significant problems that the society may face on a daily
basis. Along with emerging solutions, a problem arises when a solution has been obtained
with help of an expert knowledge and the model has not been saved and the expert is not
available, and a need arises to add new decision options.

In order to solve this problem, this thesis presents an approach of using artificial neural
networks which, after learning from previously available solutions, should be able to
evaluate new decision options. For this purpose, three types of artificial neural network
architecture are considered, namely multilayer perceptron, self-organizing map and radial
basis neural network. Two ranking similarity coefficients, namely WS and weighted
Spreaman, were used to compare the solutions proposed by the neural network. In the
quantitative and qualitative tests carried out, the results obtained showed the possibility
of using the network in low dimensionality problems with some limitations at higher
dimensionality

## License

Code is provided under [MIT](https://choosealicense.com/licenses/mit/) license
