from dataclasses import dataclass
import numpy as np

@dataclass
class FRM_dataclass:
    cvalues: int
    types: np.ndarray
    p: np.ndarray