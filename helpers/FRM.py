import numpy as np
from pymcdm import methods
import itertools


class FRM(methods.COMET):
    def __init__(self, cvalues, types, p=None, from_file=False):
        ncrit = len(types)
        ncvalues = cvalues.shape[1]
        if not from_file:
            p_tmp = []

            for crit in types:

                if crit == 1:
                    vec = list(np.random.uniform(0, 1, ncvalues)/ np.random.randint(1, ncvalues, ncvalues))
                    vec.sort()

                elif crit == -1:
                    vec = list(np.random.uniform(0, 1, ncvalues) / np.random.randint(1, ncvalues, ncvalues))
                    vec.sort(reverse=True)

                else:
                    vec = np.random.uniform(0, 1, ncvalues) / np.random.randint(1, ncvalues, ncvalues)
                    half = len(vec) // 2
                    vec_left = list(vec[:half])
                    vec_right = list(vec[half:])
                    vec_left.sort()
                    vec_right.sort(reverse=True)
                    vec[:half] = vec_left
                    vec[half:] = vec_right
                    vec = list(vec)

                p_tmp.append(vec)

            p = []
            for element in itertools.product(*p_tmp):
                p.append(sum(list(element)))

            p = np.array(p)

        self.cvalues = cvalues
        self.p = p.reshape((ncvalues ** ncrit,))
        self.p = (self.p - min(self.p)) / (max(self.p) - min(self.p))
        self.criterion_number = ncrit
        self.tfns = [self._make_tfns(chv) for chv in cvalues]