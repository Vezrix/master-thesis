import numpy as np
import pickle

no_criteria = [2, 5, 10]
no_alternatives = [10, 30, 50]

for c_n in no_criteria:
    for a_n in no_alternatives:
        M = np.random.rand(a_n, c_n)
        with open(f'./data/profit/train_data/{c_n}_{a_n}.pkl', 'wb') as outp:
            pickle.dump(M, outp, pickle.HIGHEST_PROTOCOL)

for c_n in no_criteria:
    for a_n in no_alternatives:
        M = np.random.rand(a_n, c_n)
        with open(f'./data/profit/test_data/{c_n}_{a_n}.pkl', 'wb') as outp:
            pickle.dump(M, outp, pickle.HIGHEST_PROTOCOL)