import sys
sys.path.append("..\helpers")

import pickle
from helpers.FRM import FRM
from helpers.frm_dataclass import FRM_dataclass

no_criteria = [2, 5, 10]
no_alternatives = [10, 30, 50]

for c_n in no_criteria:
    with open(f'./data/mixed/frms/frm_{c_n}.pkl', 'rb') as input_file:
        e = pickle.load(input_file)
        frm = FRM(e.cvalues, e.types, e.p, True)
    for a_n in no_alternatives:
        with open(f'./data/mixed/test_data/{c_n}_{a_n}.pkl', "rb") as input_file:
            X_test = pickle.load(input_file)
        y_test = frm(X_test)
        with open(f'./data/mixed/test_data/y_test_{c_n}_{a_n}.pkl', 'wb') as outp:
            pickle.dump(y_test, outp, pickle.HIGHEST_PROTOCOL)

        with open(f'./data/mixed/train_data/{c_n}_{a_n}.pkl', "rb") as input_file:
            X_train = pickle.load(input_file)
        y_train = frm(X_train)
        with open(f'./data/mixed/train_data/y_train_{c_n}_{a_n}.pkl', 'wb') as outp:
            pickle.dump(y_train, outp, pickle.HIGHEST_PROTOCOL)