import sys
sys.path.append("..\helpers")

import numpy as np
import pickle
from FRM import FRM
from frm_dataclass import FRM_dataclass

no_criteria = [2, 5, 10]

for no_c in no_criteria:
    types = np.ones(no_c)
    cvalues = np.array([
            [0.0, 0.2, 0.4, 0.6, 0.8, 1] for _ in range(len(types))
        ])
    frm = FRM(cvalues, types)
    frm_dataclass = FRM_dataclass(cvalues, types, frm.p)
    with open(f'C:/Studia/Mgr/data/frms/frm_{no_c}.pkl', 'wb') as outp:
        pickle.dump(frm_dataclass, outp, pickle.HIGHEST_PROTOCOL)